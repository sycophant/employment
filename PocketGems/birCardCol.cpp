
vector<int> hackerCards(vector<int> collection, int d) {

    vector<vector<int>> ans;
    vector<int> stk;
    
    function<void(int, int)> fn = [&](int i, int val) {
        if (val == 0) ans.push_back(stk); 
        else if (val > 0) 
            for (int ii = i; ii < collection.size(); ++ii) {
                stk.push_back(collection[ii]); 
                fn(ii, val - collection[ii]); 
                stk.pop_back(); 
            }
    }; 
    
    fn(0, d);
    return ans[0];
}
