We in Amazing Co need to model how our company is structured so we can do awesome stuff.

We have a root node (only one) and several children nodes, each one with its own children as well. It’s a tree-based structure.

Something like:

       root

      /    \

     a      b

We need two HTTP APIs that will serve the two basic operations:

    API

Get all(direct and non-direct) descendant nodes of a given node (the given node can be anyone in the tree structure).

Change the parent node of a given node (the given node can be anyone in the tree structure).

They need to answer quickly, even with tons of nodes. 

Also, we can’t afford to lose this information, so persistence is required.

Each node should have the following info:

a) node identification

b) who is the parent node

c) who is the root node

d) the height of the node. In the above example, height(root) = 0 and height(a) == 1.

    We can only have docker and docker-compose on our machines, so your server needs to be ran using them.

    Make a simple UI for this challenge.