#######################################################
# !/usr/bin/env python3
from flask import (
	Flask,
	jsonify,
	request,
   abort,
   session,
	render_template
)
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_mptt.mixins import BaseNestedSets

#declaration of app, db and schema instances.
app = Flask(__name__)
app.config ['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///devdb/companies.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)

# #######################################################
# # Model for database.
# # # Each node should have the following info:
# # # a) node identification
# # # b) who is the parent node
# # # c) who is the root node
# # # d) the height of the node.

class Company(db.Model, BaseNestedSets):
   __tablename__ = 'companies'
   id = db.Column(db.Integer, primary_key=True)
   name = db.Column(db.String(400), index=True, unique=True)
   items = db.relationship("Item", backref='item', lazy='dynamic')

   def __repr__(self):
      return '<Company:{} id:{} height:{} parent:{}>'.format(self.name, self.id, self.level, self.parent_id, self.tree_id)


class Item(db.Model):
   __tablename__ = 'items'
   id = db.Column(db.Integer, primary_key=True)
   Company_id = db.Column(db.Integer, db.ForeignKey('companies.id'))
   name = db.Column(db.String(475), index=True)

#######################################################
# Helper functions 
# This is a helper for drilldownTree for conversion.
def cat_to_json(item):
   return {
      'id': item.id,
      'name': item.name,
      'height': item.level,
      'parent': item.parent_id,
      'root' : item.tree_id
   }
# This takes a dict and recursively retrieves nested
# data.
def extractValues(obj, key):
   arr = []

   def extract(obj, arr, key):
      if isinstance(obj, dict):
         for k, v in obj.items():
            if isinstance(v, (dict, list)):
               extract(v, arr, key)
            elif k == key:
               arr.append(v)
      elif isinstance(obj, list):
         for item in obj:
            extract(item, arr, key)
      return arr

   results = extract(obj, arr, key)
   return results

#######################################################
# Route definitions
# They need to answer quickly, even with tons of nodes. 
# persistence is required.

@app.route("/", methods = ["GET", "POST"])
def index():
   if request.method == "GET":
      return render_template("index.html", id = '', name = '', height = '', parent = '', root = '')

   if request.method == "POST":
      # Get all(direct and non-direct) descendant nodes of a given
      # node (the given node can be anyone in the tree structure).
      if "NodeGetter" in request.form:
         try:
            #get data from form
            givenName = request.form.get('node')
            #look for parent and children database
            compNode = Company.query.filter(Company.name == givenName).one_or_none()
            drilltree = compNode.drilldown_tree(json=True, json_fields=cat_to_json)
         #if failed, raise server error.
         except:
            raise abort(500)

         #use helper function to extract values into lists.
         names = extractValues(drilltree, 'name')
         heights = extractValues(drilltree, 'height')
         parents = extractValues(drilltree, 'parent')
         ids = extractValues(drilltree, 'id')
         roots = extractValues(drilltree, 'root')

         #return webpage with values for jinja.
         return render_template("index.html", id = ids, name = names, height = heights, parent = parents, root = roots), 200
      
      # Change the parent node of a given node (the given node can
      # be anyone in the tree structure).
      if "nodeChanger" in request.form:
         try:
            givenNode = request.form.get('orig')
            givenParent = request.form.get('parent')

            nodeGiven = Company.query.filter(Company.name == givenNode).one()
            nodeParent = Company.query.filter(Company.name == givenParent).one()
         except:
            raise abort(500)

         nodeGiven.parent_id = nodeParent.id
         db.session.add(nodeGiven)
         db.session.commit()
         return render_template("index.html", id = '', name = '', height = '', parent = '', root = ''), 200

#######################################################
#Error handing happens here.
@app.errorhandler(403)
def forbidden(e):
    return jsonify({"Inaccessible!" : "URL is inaccessble"}), 403

@app.errorhandler(404)
def page_not_found(e):
    app.logger.info(f"Page not found: {request.url}")
    return jsonify({"Not found!" : "URL cannot be located"}), 404

@app.errorhandler(500)
def server_error(e):
    app.logger.error(f"Server error: {request.url}")
    return jsonify({"Server error!" : "We apologize, the server is WIP."}), 500

#######################################################
#App initialization and pre-run activity.
@app.before_first_request
def createTables():
   db.session.add(Company(name="Amazing Co"))
   db.session.add_all(
      [
         Company(name="AboutFace", parent_id=1),
         Company(name="Parakeya", parent_id=2),
         Company(name="ConnectU", parent_id=3),
      ]
   )
   db.session.add_all(
      [
         Company(name="FriendFeed", parent_id=1),
         Company(name="Octazen", parent_id=5),
         Company(name="Divvyshot", parent_id=5),
         Company(name="Friendster", parent_id=6),
         Company(name="ShareGrove", parent_id=7),
      ]
   )

   db.drop_all()
   db.create_all()
   db.session.commit()

if __name__ == '__main__':
   app.run(debug = True)