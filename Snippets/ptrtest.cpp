#include <bits/stdc++.h>

int knapSack(int wMax, int* val, int weight[], int valSize){

	std::vector<int> dpVec(wMax + 1);

	for (int i = 0; i <= wMax; i++){
		for (int j = 0; j < valSize; j++){
			if (weight[j] <= i){
				int temp = dpVec[i - weight[j]] + val[j];
				dpVec[i] = std::max(dpVec[i], temp);
			}
		}
	}

	std::sort(dpVec.begin(), dpVec.end());
	std::reverse(dpVec.begin(), dpVec.end());

	return dpVec[0];
}


int main(){

    int val[] = { 10, 20, 30 }, weight[] = { 5, 15, 10 };
    int wMax = 100, valSize = sizeof(val)/sizeof(val[0]);

    std::cout << knapSack(wMax, val, weight, valSize) << std::endl;
    return 0;
}

// int truckTour(vector<vector<int>> petrolpumps) {
    
//     vector<pair<int, int>> stops(petrolpumps.size());
//     int fuel = 0;
    
//     for (int i = 0; i < petrolpumps.size(); i++){
//         stops[i].first = petrolpumps[i][1] - petrolpumps[i][0];
//         stops[i].second = petrolpumps[i][0];
//     }
    
//     sort(stops.begin(), stops.end());
    
//     for (int i = 0; i < stops.size(); i++){
//         if (stops[i].second > fuel)
//             fuel = stops[i].second;
//     }
    
//     for (int i = 0; i < petrolpumps.size(); i++){
//         if (petrolpumps[i][0] == fuel) return i;
//     }
    
//     return -1;
// }