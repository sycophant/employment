#include <iostream>

//Sets up a templated node data structure for
//hashing.

template<typename K, typename V>

class hNode{

    public:

        V value;
        K key;

    hNode(K key, V value){

        this->value = value;
        this->key = key;
    }
};


//Templated hash table that makes use of
//the templated nodes.

template<typename K, typename V>

class HashMap{

    //hash element array
    hNode<K, V> **arr;

    //keep track of capacity and size
    unsigned int capacity;
    unsigned int size;

    hNode<K, V> *placeHolder;

    public:

    //constructor class sets up static vars, placeholder node and array for nodes.
    HashMap(){

        capacity = 20;
        size = 0;
        arr = new hNode<K,V>*[capacity];

        //fill arr with null
        for(auto i=0; i < capacity; i++) arr[i] = NULL;

        //placeholder node with value and key -1
        placeHolder = new hNode<K,V>(-1, -1);
    }

    //Return current size
    int sizeofMap() {return size;}

    //Return true if size is 0
    bool isEmpty() {return size == 0;}

    //Function to display the stored key value pairs
    void hashView(){
        
        for(int i=0 ; i<capacity ; i++){

            if(arr[i] != NULL && arr[i]->key != -1)
                std::cout
                <<"key -> " << arr[i]->key
                <<" value -> " << arr[i]->value
                << std::endl;
        }
    }

    // Function to find index for a key
    int hashCode(K key) {return key % capacity;}

    //Function to search the value for a given key
    V get(int key){

        auto hashIndex = hashCode(key);

        while(arr[hashIndex] != NULL)
        {
            int counter = 0;
            if(counter++ > capacity) return NULL;
            //if node found return its value
            if(arr[hashIndex]->key == key)
                    return arr[hashIndex]->value;
            hashIndex++;
            hashIndex %= capacity;
        }
        return NULL;
    }

    //Function to add key value pair as a node
    void insertNode(K key, V value){

        hNode<K,V> *temp = new hNode<K,V>(key, value);
        // hashCode() is called here for a key
        int hashIndex = hashCode(key);

        //find next free space
        while(arr[hashIndex] != NULL &&
            arr[hashIndex]->key != key &&
            arr[hashIndex]->key != -1){

            hashIndex++;
            hashIndex %= capacity;

        }

        //if new node to be inserted increase the current size
        if(arr[hashIndex] == NULL || arr[hashIndex]->key == -1){

                size++;
                arr[hashIndex] = temp;

            }
    }

    //Delete a node i.e a key-value pair. Takes a key
    V deleteNode(int key){

        // hashCode() is called here for a key
        auto hashIndex = hashCode(key);

        //find node using acquired key
        while(arr[hashIndex] != NULL){

            //if node found
            if(arr[hashIndex]->key == key)
            {
                hNode<K, V> *temp = arr[hashIndex];

                //Repopulate with empty node
                arr[hashIndex] = placeHolder;

                // Decrement size
                size--;
                return temp->value;
            }

            hashIndex++;
            hashIndex %= capacity;
        }

        //If not found return null
        return NULL;
    }
};

//Call the map here.
int main()
{
    HashMap<int, int> *h = new HashMap<int, int>;
    
    h->insertNode(1,1);
    h->insertNode(3,8);
    h->insertNode(2,5);
    h->hashView();

    // std::cout << h->sizeofMap() <<std::endl;
    std::cout << h->deleteNode(2) << std::endl;
    // std::cout << h->sizeofMap() <<std::endl;
    // std::cout << h->isEmpty() << std::endl;
    std::cout << h->get(2);
    std::cout << std::endl;
    h->hashView();
    std::cout << std::endl;
    return 0;
}